#include "AppDelegate.h"
#include "WelcomeScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
	// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
		glview = GLView::create("Coloriter");
		director->setOpenGLView(glview);
	}
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	director->getOpenGLView()->setFrameSize(1024.f, 576.0f);
#endif
	director->getOpenGLView()->setDesignResolutionSize(1280.f, 720.f, ResolutionPolicy::NO_BORDER);
	
	// turn on display FPS
	//director->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(
		"music/back.wav", true);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("music/fluid.wav");

	// create a scene. it's an autorelease object
	auto scene = WelcomeScene::create();

	// run
	director->runWithScene(scene);

	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
	Director::getInstance()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	 CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
	Director::getInstance()->startAnimation();

	// if you use SimpleAudioEngine, it must resume here
	CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
