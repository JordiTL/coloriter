#include "LoadingScene.h"
#include "WelcomeScene.h"
#include "LoadingLayer.h"
#include "cocos2d.h"

LoadingScene::LoadingScene(){
}


LoadingScene::~LoadingScene(){
	//CCLOG("Loading Scene deleted");
}

bool LoadingScene::init() {
	bool bRet = false;
	do{
		CC_BREAK_IF(!Scene::init());
		auto mainLayer = LoadingLayer::create();
		CC_BREAK_IF(!mainLayer);
		this->addChild(mainLayer);
		bRet = true;
	} while (0);

	return bRet;
}

void LoadingScene::onEnter(){
	Node::onEnter();

	auto scene = WelcomeScene::create();
	auto *transition = cocos2d::TransitionFade::create(1, scene);
	cocos2d::Director::getInstance()->replaceScene(transition);
}

void LoadingScene::onEnterTransitionDidFinish(){
	Node::onEnterTransitionDidFinish();
}