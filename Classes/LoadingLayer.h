#ifndef __LOADINGLAYER_H__
#define __LOADINGLAYER_H__

#include "cocos2d.h"

class LoadingLayer : public cocos2d::Layer{
private:
	cocos2d::Sprite *land1;
	cocos2d::Sprite *land2;

public:
	LoadingLayer();
	~LoadingLayer();

	virtual bool init() override;


	CREATE_FUNC(LoadingLayer);
};

#endif
