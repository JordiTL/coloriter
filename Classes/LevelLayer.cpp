#include "LevelLayer.h"

#include "cocos2d.h"

LevelLayer::LevelLayer(){

}

LevelLayer::~LevelLayer(){

}

bool LevelLayer::init(){
	bool bRet = false;
	do{
		CC_BREAK_IF(!Layer::init());
		lInit();
		scheduleUpdate();
		bRet = true;
	} while (0);

	return bRet;
}

void LevelLayer::update(float delta){
	lUpdate(delta);
}

void LevelLayer::draw(cocos2d::Renderer *renderer, const kmMat4& transform, bool transformUpdated) {
	_customCommand.init(_globalZOrder);
	_customCommand.func = CC_CALLBACK_0(LevelLayer::lDraw, this, renderer,transform, transformUpdated);
	renderer->addCommand(&_customCommand);
}