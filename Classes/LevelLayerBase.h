#ifndef __LEVELLAYERBASE_H__
#define __LEVELLAYERBASE_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "LevelLayer.h"

class GLESDebugDraw;
class FluidManager;

class LevelLayerBase : public LevelLayer{
public:
	LevelLayerBase();
	~LevelLayerBase();
	CREATE_FUNC(LevelLayerBase);

	virtual void lInit();
	virtual void lUpdate(const float delta);
	virtual void lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated);
};

#endif
