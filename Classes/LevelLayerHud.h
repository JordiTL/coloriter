#ifndef __LEVELLAYERHUD_H__
#define __LEVELLAYERHUD_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "LevelLayer.h"
#include "Map.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CCSignal.h"

class GLESDebugDraw;
class FluidManager;

class LevelLayerHud : public LevelLayer{
private:
	cocos2d::Sprite * _timeSprite;
	cocos2d::Sprite * _scoreSprite;
	cocos2d::TimerTargetCallback _timer;
	cocos2d::TimerTargetCallback _scoretimer;
	float holderWidth;
	float holderHeight;
	float timerAccum;
	float timerAccum2;
	cocos2d::Layer* m_pLayer;
	Signal<void(bool)> * _pauseFluidSignal;
	Signal<void(void)> * _removeListenersSignal;

public:
	LevelLayerHud(Signal<void(bool)>* pauseSignal, Signal<void(void)>* removeListenersSignal);
	~LevelLayerHud();

	static LevelLayerHud* create(Signal<void(bool)>* pauseSignal, Signal<void(void)>* removeListenersSignal){
		LevelLayerHud *pRet = new LevelLayerHud(pauseSignal, removeListenersSignal);
		if (pRet && pRet->init()){ 
			pRet->autorelease(); 
			return pRet; 
		}else{ 
			delete pRet; 
			pRet = NULL; 
			return NULL; 
		} 
	}

	virtual void lInit();
	virtual void lUpdate(const float delta);
	virtual void lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated);

	//void onTimeTick(float delta);
	void onTimeTick(float delta);
	void onTimeFinish(float delta);

	void goBackButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type);

};

#endif
