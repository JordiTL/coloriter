#ifndef __LEVELLAYER_H__
#define __LEVELLAYER_H__

#include "cocos2d.h"

class LevelLayer : public cocos2d::Layer{
private:
	cocos2d::CustomCommand _customCommand;

protected:
	virtual void draw(cocos2d::Renderer *renderer, const kmMat4& transform, bool transformUpdated) override;
	virtual bool init() override;
	void update(float delta) override;

public:
	LevelLayer();
	virtual ~LevelLayer();
	//CREATE_FUNC(LevelLayer);
	
	virtual void lInit() = 0;
	virtual void lUpdate(const float delta) = 0;
	virtual void lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated) = 0;
};

#endif
