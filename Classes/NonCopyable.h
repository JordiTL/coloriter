#ifndef NONCOPYABLE_H
#define NONCOPYABLE_H
class NonCopyable{
protected:
#if __cplusplus >= 201103L || defined(__GXX_EXPERIMENTAL_CXX0X__) || defined(_MSC_VER) && _MSC_VER >= 1600
	NonCopyable() = default;
	~NonCopyable() = default;
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
#else
	NonCopyable() {}
	~NonCopyable() {}
private:  // emphasize the following members are private
	NonCopyable(const NonCopyable&);
	NonCopyable& operator=(const NonCopyable&);
#endif
};
#endif