/*
 * FluidManager.cpp
 *
 *  Created on: 31/01/2014
 *      Author: Jorge
 */

#include "FluidManager.h"
#ifdef _WIN32
#include <stdlib.h>
#include <climits>
#endif
#include <cmath>


// Influence Rect location of the fluid constraints
const float FluidManager::_fluidMinX = 0.0f;
const float FluidManager::_fluidMaxX = 40.0f;
const float FluidManager::_fluidMinY = 0.0f;
const float FluidManager::_fluidMaxY = 40.0f;
unsigned int FluidUtils::_playerScore = 0u;

b2Vec2 FluidUtils::world2Screen(const float kMTP, const float maxScreenX, const float maxScreenY, const float posX, const float posY){
	float offsetX = (maxScreenX / 2) / kMTP;
	float offsetY = (maxScreenY / 2) / kMTP;
	return b2Vec2((posX + offsetX)*kMTP, (-posY + offsetY)*kMTP);
}

b2Vec2 FluidUtils::screen2World(const float kMTP, const float maxScreenX, const float maxScreenY, const float posX, const float posY){
	float offsetX = (maxScreenX);
	float offsetY = (maxScreenY);
	return b2Vec2((posX - offsetX)/kMTP, (-posY + offsetY)/kMTP);
}

b2Vec2 FluidUtils::world2Screen(const float kMTP, const b2Vec2 maxScreen, const b2Vec2 pos){
	return world2Screen(kMTP, maxScreen.x, maxScreen.y, pos.x, pos.y);
}

b2Vec2 FluidUtils::screen2World(const float kMTP, const b2Vec2 maxScreen, const b2Vec2 pos){
	return screen2World(kMTP, maxScreen.x, maxScreen.y, pos.x, pos.y);
}

b2Vec2 FluidUtils::world2Screen(const float kMTP, const float maxScreenX, const float maxScreenY, const b2Vec2 pos){
	return world2Screen(kMTP, maxScreenX, maxScreenY, pos.x, pos.y);
}

b2Vec2 FluidUtils::screen2World(const float kMTP, const float maxScreenX, const float maxScreenY, const b2Vec2 pos){
	return screen2World(kMTP, maxScreenX, maxScreenY, pos.x, pos.y);
}

b2Vec2 FluidUtils::world2Screen(const float kMTP, const b2Vec2 maxScreen, const float posX, const float posY){
	return world2Screen(kMTP, maxScreen.x, maxScreen.y, posX, posY);
}

b2Vec2 FluidUtils::screen2World(const float kMTP, const b2Vec2 maxScreen, const float posX, const float posY){
	return screen2World(kMTP, maxScreen.x, maxScreen.y, posX, posY);
}

// Move the particle from inside a body to the nearest point outside, and (if appropriate), adjust
// the particle's velocity
void FluidUtils::separateParticleFromBody(int particleIdx,
	b2Vec2& nearestPos, b2Vec2& normal, sParticle *liquid) {
	liquid[particleIdx].Position = nearestPos;
}

bool FluidUtils::particleSolidCollision(b2Fixture* fixture,
	b2Vec2& particlePos, b2Vec2& nearestPos, b2Vec2& impactNormal, sParticle * particle) {
	const float particleRadius = 0.2f;

	if (fixture->GetShape()->GetType() == b2Shape::e_circle) {
		b2CircleShape* pCircleShape =
			static_cast<b2CircleShape*>(fixture->GetShape());
		const b2Transform& xf = fixture->GetBody()->GetTransform();
		float radius = pCircleShape->m_radius + particleRadius;
		b2Vec2 circlePos = xf.p + pCircleShape->m_p;
		b2Vec2 delta = particlePos - circlePos;
		if (delta.LengthSquared() > radius * radius) {
			return false;
		}

		delta.Normalize();
		delta *= radius;
		nearestPos = delta + pCircleShape->m_p;
		impactNormal = (nearestPos - circlePos);
		impactNormal.Normalize();

		return true;

	}
	else if (fixture->GetShape()->GetType() == b2Shape::e_polygon) {
		if (fixture->IsSensor()){
			
				_playerScore++;
				particle->Eaten = true;
				//CCLOG("Puntuacion %i:", _playerScore);
				return false;
			
		}

		b2PolygonShape* pPolyShape =
			static_cast<b2PolygonShape*>(fixture->GetShape());
		const b2Transform& xf = fixture->GetBody()->GetTransform();
		int numVerts = pPolyShape->GetVertexCount();

		b2Vec2 vertices[b2_maxPolygonVertices];
		b2Vec2 normals[b2_maxPolygonVertices];

		for (int32 i = 0; i < numVerts; ++i) {
			vertices[i] = b2Mul(xf, pPolyShape->m_vertices[i]);
			normals[i] = b2Mul(xf.q, pPolyShape->m_normals[i]);
		}

		float shortestDistance = 99999.0f;

		for (int i = 0; i < numVerts; ++i) {
			b2Vec2 vertex = vertices[i] + particleRadius * normals[i]
				- particlePos;
			float distance = b2Dot(normals[i], vertex);

			if (distance < 0.0f) {
				return false;
			}

			if (distance < shortestDistance) {
				shortestDistance = distance;

				nearestPos = b2Vec2(normals[i].x * distance + particlePos.x,
					normals[i].y * distance + particlePos.y);

				impactNormal = normals[i];
			}
		}

		return true;
	}
	else {
		// Unrecognised shape type
		assert(false);
		return false;
	}
}



void FluidManager::_stepFluidParticles(float deltaT) {
	float dt2 = deltaT * deltaT;
	for (int i = 0; i < nParticles; ++i) {

		b2Vec2 acc = (_liquid[i].Force + _world->GetGravity());

		// electrodruid: Should mass play a part in this?
		//  	acc.x /= liquid[i].mMass;
		//  	acc.y /= liquid[i].mMass;

		acc.x *= dt2;
		acc.y *= dt2;

		//		liquid[i].mVelocity = liquid[i].mPosition - liquid[i].mOldPosition + (acc * dt2);
		_liquid[i].Velocity = _liquid[i].Position - _liquid[i].LastPosition
			+ acc;

		// Dampen velocity
		_liquid[i].Velocity.x *= 0.995f;
		_liquid[i].Velocity.y *= 0.995f;

		_liquid[i].LastPosition = _liquid[i].Position;
		_liquid[i].Position += _liquid[i].Velocity;

		_liquid[i].sp->setPosition(
			cocos2d::Point(32.f * _liquid[i].Position.x,
			MTP_VALUE * _liquid[i].Position.y));
		//        liquid[i].sp.rotation = -1.f * CC_RADIANS_TO_DEGREES(ccpToAngle(ccp(liquid[i].mVelocity.x, liquid[i].mVelocity.y)));
		//        liquid[i].sp.scaleX = clampf(liquid[i].mVelocity.LengthSquared() * 10.f, 1.f, 4.f);
	}
}

bool FluidManager::QueryWorldInteractions::ReportFixture(b2Fixture* fixture) {


	int numParticles = hashGridList[x][y].GetSize();
	hashGridList[x][y].ResetIterator();

	// Iterate through all the particles in this cell
	for (int i = 0; i < numParticles; i++) {
		int particleIdx = hashGridList[x][y].GetNext();

		b2Vec2 particlePos = liquid[particleIdx].Position;
		if (fixture->GetBody()->GetType() == b2_staticBody) {
			b2Vec2 nearestPos(0, 0);
			b2Vec2 normal(0, 0);


			bool inside = FluidUtils::particleSolidCollision(fixture, particlePos, nearestPos, normal, &liquid[particleIdx]);

			if (inside) {
				FluidUtils::separateParticleFromBody(particleIdx, nearestPos, normal,
					liquid);
			}
		}
		else {
			b2Vec2 nearestPos(0, 0);
			b2Vec2 normal(0, 0);
			bool inside = FluidUtils::particleSolidCollision(fixture, particlePos,
				nearestPos, normal,&liquid[particleIdx]);

			if (inside) {
				b2Vec2 particleVelocity = liquid[particleIdx].Velocity;


				particleVelocity *= deltaT;

				b2Vec2 impulsePos = particlePos;
				//									b2Vec2 impulsePos = nearestPos;

				b2Vec2 pointVelocity =
					fixture->GetBody()->GetLinearVelocityFromWorldPoint(
					impulsePos);
				b2Vec2 pointVelocityAbsolute = pointVelocity;
				
				pointVelocity *= deltaT;

				b2Vec2 relativeVelocity = particleVelocity - pointVelocity;

				b2Vec2 pointVelNormal = normal;
				pointVelNormal *= b2Dot(relativeVelocity, normal);
				b2Vec2 pointVelTangent = relativeVelocity - pointVelNormal;

				// Should be a value between 0.0f and 1.0f
				const float slipFriction = 0.3f;

				pointVelTangent *= slipFriction;
				b2Vec2 impulse = pointVelNormal - pointVelTangent;


				fixture->GetBody()->ApplyLinearImpulse(impulse, impulsePos, true);
				//									 pShape->GetBody()->ApplyForce(impulse, impulsePos);

				b2Vec2 buoyancy = b2Vec2(0, 10.f);
				const float buoyancyAdjuster = 0.f;
				buoyancy *= buoyancyAdjuster;

				fixture->GetBody()->ApplyForce(buoyancy, fixture->GetBody()->GetPosition(), true);

				// move the particles away from the body

				FluidUtils::separateParticleFromBody(particleIdx, nearestPos, normal, liquid);

				liquid[particleIdx].Velocity -= impulse;
				liquid[particleIdx].Velocity += pointVelocityAbsolute;

			}
		}
	}
	return true;
}

bool FluidManager::QueryWorldPostIntersect::ReportFixture(b2Fixture *fixture) {
	int numParticles = hashGridList[x][y].GetSize();
	hashGridList[x][y].ResetIterator();

	for (int i = 0; i < numParticles; i++) {
		int particleIdx = hashGridList[x][y].GetNext();

		b2Vec2 particlePos = liquid[particleIdx].Position;
		if (fixture->GetBody()->GetType() == b2_dynamicBody) {
			b2Vec2 nearestPos(0, 0);
			b2Vec2 normal(0, 0);
			bool inside = FluidUtils::particleSolidCollision(fixture, particlePos,
				nearestPos, normal, &liquid[particleIdx]);

			if (inside) {
				FluidUtils::separateParticleFromBody(particleIdx, nearestPos, normal,
					liquid);
			}
		}
	}
	return true;
}

FluidManager::~FluidManager() {
	//	clearHashGrid();
	//delete _world;
	delete _intersectQueryCallback;
}

FluidManager::FluidManager(cocos2d::Node* layer, b2World * world) :
_rad(1.6f), _visc(0.002f), _idealRad(50.0f), _totalMass(30.f), _boxWidth(
5.f), _boxHeight(5.f), _spawnLocation(.0f,.0f){

#ifdef _WIN32
	srand(time(NULL));
#endif
#ifdef __ANDROID__
	srandom(time(NULL));
#endif

	_layer = layer;
	_world = world;
	// TODO Implement new SpriteChange method
	_particleSprites = cocos2d::SpriteBatchNode::create("blue_circle.png");
	_layer->addChild(_particleSprites,10,100);
	_createStaticGeometry();

	_intersectQueryCallback = new QueryWorldInteractions(_hashGridList, _liquid);
	_eulerIntersectQueryCallback = new QueryWorldPostIntersect(_hashGridList, _liquid);
}

void FluidManager::loadColliders(const std::vector<Collider>& colliders){
	// Physics body creation centered at (0,0)
	b2BodyDef bd;
	bd.type = b2_staticBody;
	bd.position.Set(.0f,.0f);
	b2Body* ground = _world->CreateBody(&bd);
	
	b2PolygonShape sd;
	b2FixtureDef gFix;
	gFix.shape = &sd;
	
	for (auto collider : colliders){
		

		float cosa = std::cos(-collider.rotation* M_PI / 180.f);
		float sina = std::sin(-collider.rotation* M_PI / 180.f);


		float hx = (collider.width / 2) / MTP_VALUE;
		float hy = (collider.height / 2) / MTP_VALUE;
		float cx = (collider.x + (collider.width / 2)*cosa - (collider.height / 2)*sina) / MTP_VALUE;
		float cy = (collider.y + (collider.width / 2)*sina + (collider.height / 2)*cosa) / MTP_VALUE;

		sd.SetAsBox(hx, hy, b2Vec2(cx, cy), -collider.rotation * M_PI / 180.f);
		ground->CreateFixture(&gFix);
	}

}

void FluidManager::setSpawnPosition(const b2Vec2 pos){
	_spawnLocation = pos;
}

void FluidManager::_createStaticGeometry() {
	//_world = new b2World(b2Vec2(0.f, -10.f));

	_debugDraw = new GLESDebugDraw(MTP_VALUE);
	_world->SetDebugDraw(_debugDraw);

	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	//		flags += b2Draw::e_jointBit;
			flags += b2Draw::e_aabbBit;
	//		flags += b2Draw::e_pairBit;
			flags += b2Draw::e_centerOfMassBit;
	_debugDraw->SetFlags(flags);

	// Static geometry
	b2PolygonShape sd;
	sd.SetAsBox(5.0f, 0.5f);
	
	cocos2d::Size s = cocos2d::Director::getInstance()->getWinSize();
	/*b2BodyDef bd;
	bd.type = b2_staticBody;
	//bd.position = FluidUtils::screen2World(32, winSize.width, winSize.height, _spawnLocation.x, _spawnLocation.y);
	bd.position.Set((s.width / 2) / MTP_VALUE, 1.0f);
	b2Body* ground = _world->CreateBody(&bd);*/

	/*b2FixtureDef gFix;
	gFix.shape = &sd;
	sd.SetAsBox(3.0f, 0.4f);

	ground->CreateFixture(&gFix);
	sd.SetAsBox(1.0f, 0.8f, b2Vec2(_spawnLocation.y / 32 + 0.0f, 0.0f), -0.2f);
	ground->CreateFixture(&gFix);
	sd.SetAsBox(1.5f, 0.2f, b2Vec2(_spawnLocation.x / 32 + -1.2f, _spawnLocation.y / 32 + 5.2f), -1.5f);
	ground->CreateFixture(&gFix);
	sd.SetAsBox(0.5f, 50.0f, b2Vec2(_spawnLocation.x / 32 + 5.0f, _spawnLocation.y / 32 + 0.0f), 0.0f);
	ground->CreateFixture(&gFix);

	sd.SetAsBox(0.5f, 3.0f, b2Vec2(_spawnLocation.x / 32 + -8.0f, _spawnLocation.y / 32 + 0.0f), 0.0f);
	ground->CreateFixture(&gFix);

	sd.SetAsBox(2.0f, 0.1f, b2Vec2(_spawnLocation.x / 32 + -6.0f, _spawnLocation.y / 32 + -2.8f), 0.1f);
	ground->CreateFixture(&gFix);*/


	//  	sd.SetAsBox(0.3f,2.0f,b2Vec2(-4.5f,-2.0f),0.0f);
	//  	ground->CreateFixture(&gFix);

	/*b2CircleShape cd;
	cd.m_radius = 0.5f;
	cd.m_p = b2Vec2(-0.5f, -4.0f);
	gFix.shape = &cd;
	ground->CreateFixture(&gFix);*/

	// Particles
	float massPerParticle = _totalMass / nParticles;

	float cx = 0.0f;
	float cy = 0.0f;

	for (int i = 0; i < nParticles; ++i) {
		_liquid[i].Position = b2Vec2(
			random(cx - _boxWidth * .5f, cx + _boxWidth * .5f),
			random(cy - _boxHeight * .5f, cy + _boxHeight * .5f));

		//_liquid[i].Position = FluidUtils::screen2World(32, winSize.width, winSize.height, _spawnLocation.x, _spawnLocation.y);
		_liquid[i].Position.x += _spawnLocation.x / MTP_VALUE;
		_liquid[i].Position.y += _spawnLocation.y / MTP_VALUE;

		_liquid[i].LastPosition = _liquid[i].Position;
		_liquid[i].Velocity = b2Vec2(0.0f, 0.0f);
		_liquid[i].Acceleration = b2Vec2(0, -10.0f);

		_liquid[i].Mass = massPerParticle;
		_liquid[i].Restitution = 0.4f;
		_liquid[i].Friction = 0.0f;
		_liquid[i].Eaten = false;
		cocos2d::Sprite *sp = cocos2d::Sprite::create("blue_circle.png");
		_particleSprites->addChild(sp);

		_liquid[i].sp = sp;
		sp->retain();
		// TO-DO 　　　　　　　 LO ESTAS RETENIENDO !!!!!!!!  LIBERAD
	}

	// Box
	//b2FixtureDef polyDef;
	//b2PolygonDef polyDef;
	//b2PolygonShape shape;
	//shape.SetAsBox(3.0f, 0.4f);



	//	polyDef.density = 1.0f;
	//polyDef.density = 4.0f;
	//b2BodyDef bodyDef;
	//bodyDef.type = b2_dynamicBody;
	//bodyDef.position = b2Vec2((s.width / 2) / MTP_VALUE, 10.0f);

	//bodyDef.angularDamping = 0.5f;

	//_bod = _world->CreateBody(&bodyDef);
	//polyDef.shape = &shape;
	//_bod->CreateFixture(&polyDef);
}

void FluidManager::draw() {
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	cocos2d::ccGLEnableVertexAttribs(cocos2d::kCCVertexAttribFlag_Position);

	kmGLPushMatrix();

	_world->DrawDebugData();

	kmGLPopMatrix();
}

void FluidManager::update(float dt) {

	// Get initial positions
	_hashLocations();
	// Accumulate forces and apply constraints
	_applyLiquidConstraint(dt);
	_processWorldInteractions(dt);

	// Update fluid positions
	_stepFluidParticles(dt);

	// Update box2d positions
	_world->Step(dt, 8, 3);

	// Does nothing under Verlet, we've already handled damping in stepFluidParticles
	_dampenLiquid();

	_checkBounds();
}

void FluidManager::_clearHashGrid() {
	for (int a = 0; a < hashWidth; a++) {
		for (int b = 0; b < hashHeight; b++) {
			_hashGridList[a][b].Clear();
		}
	}
}

void FluidManager::_hashLocations() {
	_clearHashGrid();

	for (int a = 0; a < nParticles; a++) {
		int hcell = hashX(_liquid[a].Position.x);
		int vcell = hashY(_liquid[a].Position.y);

		if (hcell > -1 && hcell < hashWidth && vcell > -1 && vcell < hashHeight) {
			_hashGridList[hcell][vcell].PushBack(a);
		}
	}
}


void FluidManager::_resetGridTailPointers(int particleIdx) {
	int hcell = hashX(_liquid[particleIdx].Position.x);
	int vcell = hashY(_liquid[particleIdx].Position.y);

	for (int nx = -1; nx < 2; nx++) {
		for (int ny = -1; ny < 2; ny++) {
			int xc = hcell + nx;
			int yc = vcell + ny;

			if (xc > -1 && xc < hashWidth && yc > -1 && yc < hashHeight) {
				if (!_hashGridList[xc][yc].IsEmpty()) {
					_hashGridList[xc][yc].UnSplice();
				}
			}
		}
	}
}

void FluidManager::_applyLiquidConstraint(float deltaT) {


	float multiplier = _idealRad / _rad;

	float xchange[nParticles] = { 0.0f };
	float ychange[nParticles] = { 0.0f };

	float xs[nParticles];
	float ys[nParticles];
	float vxs[nParticles];
	float vys[nParticles];

	for (int i = 0; i < nParticles; ++i) {
		xs[i] = multiplier * _liquid[i].Position.x;
		ys[i] = multiplier * _liquid[i].Position.y;
		vxs[i] = multiplier * _liquid[i].Velocity.x;
		vys[i] = multiplier * _liquid[i].Velocity.y;
	}

	cFluidHashList neighbours;

	float* vlen = _vlenBuffer;

	for (int i = 0; i < nParticles; i++) {
		// Populate the neighbor list from the 9 proximate cells
		int hcell = hashX(_liquid[i].Position.x);
		int vcell = hashY(_liquid[i].Position.y);

		bool bFoundFirstCell = false;
		for (int nx = -1; nx < 2; nx++) {
			for (int ny = -1; ny < 2; ny++) {
				int xc = hcell + nx;
				int yc = vcell + ny;
				if (xc > -1 && xc < hashWidth && yc > -1 && yc < hashHeight) {
					if (!_hashGridList[xc][yc].IsEmpty()) {
						if (!bFoundFirstCell) {
							// Set the head and tail of the beginning of our neighbours list
							neighbours.SetHead(_hashGridList[xc][yc].pHead());
							neighbours.SetTail(_hashGridList[xc][yc].pTail());
							bFoundFirstCell = true;
						}
						else {
							// We already have a neighbours list, so just add this cell's particles onto
							// the end of it.
							neighbours.Splice(_hashGridList[xc][yc].pHead(),
								_hashGridList[xc][yc].pTail());
						}
					}
				}
			}
		}

		int neighboursListSize = neighbours.GetSize();
		neighbours.ResetIterator();

		// Particle pressure calculated by particle proximity
		// Pressures = 0 if all particles within range are idealRad distance away
		float p = 0.0f;
		float pnear = 0.0f;
		for (int a = 0; a < neighboursListSize; a++) {
			int n = neighbours.GetNext();

			int j = n;

			float vx = xs[j] - xs[i];//liquid[j]->GetWorldCenter().x - liquid[i]->GetWorldCenter().x;
			float vy = ys[j] - ys[i];//liquid[j]->GetWorldCenter().y - liquid[i]->GetWorldCenter().y;

			//early exit check
			if (vx > -_idealRad && vx < _idealRad && vy > -_idealRad
				&& vy < _idealRad) {
				float vlensqr = (vx * vx + vy * vy);
				//within idealRad check
				if (vlensqr < _idealRad * _idealRad) {
					vlen[a] = b2Sqrt(vlensqr);
					if (vlen[a] < b2_linearSlop) {
						//						vlen[a] = idealRad-.01f;
						vlen[a] = b2_linearSlop;
					}
					float oneminusq = 1.0f - (vlen[a] / _idealRad);
					p = (p + oneminusq * oneminusq);
					pnear = (pnear + oneminusq * oneminusq * oneminusq);
				}
				else {

#ifdef _WIN32
					vlen[a] = FLT_MAX;
#endif
#ifdef __ANDROID__
					vlen[a] = MAXFLOAT;
#endif


				}
			}
		}

		// Now actually apply the forces
		float pressure = (p - 5.0f) / 2.0f;	//normal pressure term
		float presnear = pnear / 2.0f;	//near particles term
		float changex = 0.0f;
		float changey = 0.0f;

		neighbours.ResetIterator();

		for (int a = 0; a < neighboursListSize; a++) {
			int n = neighbours.GetNext();

			int j = n;

			float vx = xs[j] - xs[i]; //liquid[j]->GetWorldCenter().x - liquid[i]->GetWorldCenter().x;
			float vy = ys[j] - ys[i]; //liquid[j]->GetWorldCenter().y - liquid[i]->GetWorldCenter().y;
			if (vx > -_idealRad && vx < _idealRad && vy > -_idealRad
				&& vy < _idealRad) {
				if (vlen[a] < _idealRad) {
					float q = vlen[a] / _idealRad;
					float oneminusq = 1.0f - q;
					float factor = oneminusq * (pressure + presnear * oneminusq)
						/ (2.0f * vlen[a]);
					float dx = vx * factor;
					float dy = vy * factor;
					float relvx = vxs[j] - vxs[i];
					float relvy = vys[j] - vys[i];
					factor = _visc * oneminusq * deltaT;
					dx -= relvx * factor;
					dy -= relvy * factor;

					xchange[j] += dx;
					ychange[j] += dy;
					changex -= dx;
					changey -= dy;
				}
			}
		}

		xchange[i] += changex;
		ychange[i] += changey;

		// We've finished with this neighbours list, so go back and re-null-terminate all of the
		// grid cells lists ready for the next particle's neighbours list.
		_resetGridTailPointers(i);
	}

	for (int i = 0; i < nParticles; ++i) {
		_liquid[i].Position += b2Vec2(xchange[i] / multiplier,
			ychange[i] / multiplier);
	}
}

void FluidManager::_checkBounds() {
	float massPerParticle = _totalMass / nParticles;

	for (int i = 0; i < nParticles; ++i) {
		if (_liquid[i].Position.y < 0.0f || _liquid[i].Eaten) {
			//float cx = 0.0f + random(-0.6f, 0.6f);
			//float cy = 15.0f + random(-2.3f, 2.0f);

			float cx = _spawnLocation.x / MTP_VALUE + random(-0.6f, 0.6f);
			float cy = _spawnLocation.y / MTP_VALUE + random(-2.3f, 2.0f);

			
			//_liquid[i].Position = FluidUtils::screen2World(32, winSize.width, winSize.height, _spawnLocation.x + random(-0.6f, 0.6f), _spawnLocation.y + random(-2.3f, 2.0f));
			_liquid[i].Position = b2Vec2(cx, cy);
			_liquid[i].LastPosition = _liquid[i].Position;
			_liquid[i].Velocity = b2Vec2(0.0f, 0.0f);
			_liquid[i].Acceleration = b2Vec2(0, -10.0f);
			_liquid[i].Eaten = false;
			_liquid[i].Mass = massPerParticle;
			_liquid[i].Restitution = 0.4f;
			_liquid[i].Friction = 0.0f;
		}
	}

	/*if (_bod->GetWorldCenter().y < 0.0f) {
		_world->DestroyBody(_bod);
		b2PolygonShape polyShape;
		//		b2PolygonDef polyDef;
		polyShape.SetAsBox(random(0.3f, 0.7f), random(0.3f, 0.7f));

		b2FixtureDef fixDef;
		fixDef.density = 4.0f;
		fixDef.shape = &polyShape;

		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		
		
		bodyDef.position = b2Vec2(_spawnLocation.x / 2, _spawnLocation.y / 2);
		_bod = _world->CreateBody(&bodyDef);
		_bod->CreateFixture(&fixDef);
		//		bod->SetMassFromShapes();
	}*/
}

void FluidManager::_dampenLiquid() {
	for (int i = 0; i < nParticles; ++i) {
		_liquid[i].Velocity.x *= 0.995f;
		_liquid[i].Velocity.y *= 0.995f;
	}
}

// Handle interactions with the world
void FluidManager::_processWorldInteractions(float deltaT) {
	// Iterate through the grid, and do an AABB test for every grid containing particles
	for (int x = 0; x < hashWidth; ++x) {
		for (int y = 0; y < hashWidth; ++y) {
			if (!_hashGridList[x][y].IsEmpty()) {
				float minX = mapValue((float)x, 0, hashWidth, _fluidMinX,
					_fluidMaxX);
				float maxX = mapValue((float)x + 1, 0, hashWidth, _fluidMinX,
					_fluidMaxX);
				float minY = mapValue((float)y, 0, hashHeight, _fluidMinY,
					_fluidMaxY);
				float maxY = mapValue((float)y + 1, 0, hashHeight, _fluidMinY,
					_fluidMaxY);

				b2AABB aabb;

				aabb.lowerBound.Set(minX, minY);
				aabb.upperBound.Set(maxX, maxY);

				_intersectQueryCallback->x = x;
				_intersectQueryCallback->y = y;
				_intersectQueryCallback->deltaT = deltaT;
				_world->QueryAABB(_intersectQueryCallback, aabb);
			}
		}
	}
}

// Detect an intersection between a particle and a b2Shape, and also try to suggest the nearest
// point on the shape to move the particle to, and the shape normal at that point



void FluidManager::_resolveIntersections(float deltaT) {
	// Iterate through the grid, and do an AABB test for every grid containing particles
	for (int x = 0; x < hashWidth; ++x) {
		for (int y = 0; y < hashWidth; ++y) {
			if (!_hashGridList[x][y].IsEmpty()) {
				float minX = mapValue((float)x, 0, hashWidth, _fluidMinX,
					_fluidMaxX);
				float maxX = mapValue((float)x + 1, 0, hashWidth, _fluidMinX,
					_fluidMaxX);
				float minY = mapValue((float)y, 0, hashHeight, _fluidMinY,
					_fluidMaxY);
				float maxY = mapValue((float)y + 1, 0, hashHeight, _fluidMinY,
					_fluidMaxY);

				b2AABB aabb;

				aabb.lowerBound.Set(minX, minY);
				aabb.upperBound.Set(maxX, maxY);

				_eulerIntersectQueryCallback->x = x;
				_eulerIntersectQueryCallback->y = y;
				_eulerIntersectQueryCallback->deltaT = deltaT;
				_world->QueryAABB(_eulerIntersectQueryCallback, aabb);
			}
		}
	}
}
