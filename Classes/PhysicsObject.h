#ifndef __PHYSICSOBJECT_H__
#define __PHYSICSOBJECT_H__

#include "Collider.h"
#include <string>

class PhysicsObject: public Collider{
public:
	std::string filename;
	std::string physicsDefFilename;
	std::string physicsId;
	float rotationSpeed;
	float translationSpeed;

	unsigned int userTag;

public:
	PhysicsObject(float px = .0f, float py = .0f, float w = 1.f, float h = 1.f, float r = .0f) : Collider(px,py,w, h, r), rotationSpeed(0.f), translationSpeed(0.f), userTag(0u){}

	~PhysicsObject(){}
};

#endif
