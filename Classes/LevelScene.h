#ifndef __LEVELSCENE_H__
#define __LEVELSCENE_H__
#include "cocos2d.h"
#include "CCSignal.h"
#include "FluidManager.h"
#include "Map.h"
#include "Box2D/Box2D.h"

class LevelScene : public cocos2d::Scene{
private:
	Signal<void(bool)> _pauseFluidSignal;
	Signal<void(void)> _removeListenersSignal;
	Map _map;
	FluidManager *_fluidMgr;
	b2World *_world;

public:
	LevelScene();
	~LevelScene();

	void onEnter() override;
	void onEnterTransitionDidFinish() override;

	bool init() override;
	//CREATE_FUNC(LevelScene);
	void loadMap(const std::string& filename);
	static LevelScene* create()
	{
		LevelScene *pRet = new LevelScene();
		if (pRet && pRet->init())
		{
			pRet->autorelease();
			return pRet;
		}
		else
		{
			delete pRet;
			pRet = NULL;
			return NULL;
		}
	}

};

#endif

