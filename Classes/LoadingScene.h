#ifndef __LOADINGSCENE_H__
#define __LOADINGSCENE_H__

#include "cocos2d.h"

class LoadingScene : public cocos2d::Scene{
public:
	LoadingScene();
	~LoadingScene();

	virtual bool init() override;
	void onEnter() override;
	void onEnterTransitionDidFinish() override;

	CREATE_FUNC(LoadingScene);
};

#endif
