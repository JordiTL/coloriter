#ifndef __CCSIGNAL_H__
#define __CCSIGNAL_H__
#include <functional>
#include <vector>

template<typename Sig>
class Signal;

template<typename R, typename... Args>
class Signal<R(Args...)>{
public:
	typedef std::function<R(Args...)> Slot;

private:
	std::vector<Slot> _slots;

public:
	Signal();
	~Signal();

	void connect(Slot slot);
	void disconnectAll();
	void emit(Args... args) const;
};

template<typename R, typename... Args>
void Signal<R(Args...)>::emit(Args... args) const{
	for (std::function<R(Args...)> func : _slots)
		func(std::forward<Args>(args)...);
}

template<typename R, typename... Args>
Signal<R(Args...)>::Signal(){
}

template<typename R, typename... Args>
Signal<R(Args...)>::~Signal(){
}

template<typename R, typename... Args>
void Signal<R(Args...)>::connect(Slot slot){
	_slots.push_back(slot);
}

template<typename R, typename... Args>
void Signal<R(Args...)>::disconnectAll(){
	_slots.clear();
}

#endif
