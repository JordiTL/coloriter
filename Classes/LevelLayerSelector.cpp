#include "LevelLayerSelector.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "LevelScene.h"
#include "LevelSelectorScene.h"

LevelLayerSelector::LevelLayerSelector(){
}


LevelLayerSelector::~LevelLayerSelector(){
	CCLOG("Welcome Layer deleted");
}


bool LevelLayerSelector::init(){
	if (!Layer::init()){
		return false;
	}

	cocos2d::Layer* m_pLayer = cocos2d::Layer::create();
	addChild(m_pLayer);

	cocos2d::ui::Layout* m_pLayout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ui/NewUI3/NewUI3_1.json"));

	m_pLayer->addChild(m_pLayout);
	cocos2d::ui::Widget *level1 = static_cast<cocos2d::ui::Widget*>(m_pLayout->getChildByName("BG")->getChildByName("level1"));
	cocos2d::ui::Widget *level2 = static_cast<cocos2d::ui::Widget*>(m_pLayout->getChildByName("BG")->getChildByName("level2"));

	level1->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)&LevelLayerSelector::level1Action);
	level2->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)&LevelLayerSelector::level2Action);
	return true;
}

void LevelLayerSelector::level1Action(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type){
	switch (type)
	{
	case cocos2d::ui::TOUCH_EVENT_BEGAN:{
											auto scene = LevelScene::create();
											scene->loadMap("Maps/Map2/map1.tmx");
											auto *transition = cocos2d::TransitionFade::create(1, scene);
											cocos2d::Director::getInstance()->replaceScene(scene);
											break;
	}
	case cocos2d::ui::TOUCH_EVENT_MOVED: {
											 // TODO
											 break;
	}
	case cocos2d::ui::TOUCH_EVENT_ENDED:{
											// TODO
											break;
	}
	case cocos2d::ui::TOUCH_EVENT_CANCELED:{
											   // TODO
											   break;
	}
	default:{
				// TODO
				break;
	}
	}
}

void LevelLayerSelector::level2Action(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type){
	switch (type)
	{
	case cocos2d::ui::TOUCH_EVENT_BEGAN:{
											auto scene = LevelScene::create();
											scene->loadMap("Maps/Map2/mapa2.tmx");
											auto *transition = cocos2d::TransitionFade::create(1, scene);
											cocos2d::Director::getInstance()->replaceScene(scene);
											break;
	}
	case cocos2d::ui::TOUCH_EVENT_MOVED: {
											 // TODO
											 break;
	}
	case cocos2d::ui::TOUCH_EVENT_ENDED:{
											// TODO
											break;
	}
	case cocos2d::ui::TOUCH_EVENT_CANCELED:{
											   // TODO
											   break;
	}
	default:{
				// TODO
				break;
	}
	}
}