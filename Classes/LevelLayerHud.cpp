#include "LevelLayerHud.h"
#include "FluidManager.h"
#include "GLES-Render.h"
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "MapManager.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "LevelSelectorScene.h"
#include "SimpleAudioEngine.h"

LevelLayerHud::LevelLayerHud(Signal<void(bool)>* pauseSignal, Signal<void(void)>* removeListenersSignal) : _pauseFluidSignal(pauseSignal), _removeListenersSignal(removeListenersSignal){
}

LevelLayerHud ::~LevelLayerHud(){

}

void LevelLayerHud::lInit(){
	m_pLayer = cocos2d::Layer::create();
	addChild(m_pLayer);

	FluidUtils::setPlayerScore(0u);
	cocos2d::ui::Layout* _layout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ui/HudColoriter/HudColoriter.json"));
	this->setSwallowsTouches(false);
	this->setTouchEnabled(true);
	m_pLayer->setSwallowsTouches(false);
	m_pLayer->setTouchEnabled(true);

	_layout->setTouchEnabled(false);
	m_pLayer->addChild(_layout);

	/*cocos2d::Texture2D * fluidTex = cocos2d::TextureCache::getInstance()->addImage("fluidTex.png");
	cocos2d::Texture2D::TexParams texParams = {
		GL_NEAREST,
		GL_NEAREST,
		GL_REPEAT,
		GL_REPEAT
	};
	fluidTex->setTexParameters(texParams);

	_timeSprite = cocos2d::Sprite::createWithTexture(fluidTex);
	_scoreSprite = cocos2d::Sprite::createWithTexture(fluidTex);

	this->addChild(_timeSprite);
	this->addChild(_scoreSprite);


	//m_pLayer->getChildByTag(22)->getChildByTag(24)->setAnchorPoint(cocos2d::Point(.0f, 1.f));
	//m_pLayer->getChildByTag(22)->getChildByTag(25)->setAnchorPoint(cocos2d::Point(.0f, 1.f));
	//_timeSprite->setAnchorPoint(cocos2d::Point(.0f, 1.f));
	//_scoreSprite->setAnchorPoint(cocos2d::Point(.0f, 1.f));

	_timeSprite->setPosition(m_pLayer->getChildByTag(22)->getChildByTag(24)->getPosition());
	_scoreSprite->setPosition(m_pLayer->getChildByTag(22)->getChildByTag(25)->getPosition());
	//_timeSprite->setContentSize(m_pLayer->getChildByTag(22)->getChildByTag(24)->getContentSize());
	//_scoreSprite->setContentSize(m_pLayer->getChildByTag(22)->getChildByTag(25)->getContentSize());

	_timer.initWithCallback(this->getScheduler(), [this](float time){
		const float desirableW = 0.1f + _timeSprite->getContentSize().width / 150.f;
		const float desirableY = 0.1f + _timeSprite->getContentSize().height / 150.f;

		float newScaleX = (desirableW * _timeSprite->getScaleX()) / _timeSprite->getContentSize().width;
		float newScaleY = (desirableY * _timeSprite->getScaleY()) / _timeSprite->getContentSize().height;

		_timeSprite->setScaleX(newScaleX);
	}, this, "TimeOut", 0.1f, 150u, .0f);
	*/
	this->schedule(schedule_selector(LevelLayerHud::onTimeTick), .1f);
	this->schedule(schedule_selector(LevelLayerHud::onTimeFinish), .1f);
	timerAccum = 0.0f;
	timerAccum2 = 0.0f;
}

void LevelLayerHud::lUpdate(const float delta){
	_timer.update(delta);

	if (FluidUtils::getPlayerScore() < 250.f){
		/*const float incrementH = holderHeight / 250.f;
		float newScaleY = ((_scoreSprite->getContentSize().height)*FluidUtils::getPlayerScore()/250.f * _timeSprite->getScaleY()) / _scoreSprite->getContentSize().height;

		_scoreSprite->setScaleY(newScaleY);
		_scoreSprite->setContentSize(cocos2d::Size(_scoreSprite->getContentSize().width + incrementH, _scoreSprite->getContentSize().height));*/

		static_cast<cocos2d::ui::LoadingBar*>(m_pLayer->getChildByTag(3)->getChildByTag(8))->setPercent((FluidUtils::getPlayerScore() / 250.f) * 100.f);
	}

}

void LevelLayerHud::lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated){
}

void LevelLayerHud::onTimeTick(float delta){
	timerAccum += 0.1f;
	if (timerAccum > 15.f){
		this->unschedule(schedule_selector(LevelLayerHud::onTimeTick));
		_pauseFluidSignal->emit(false);
		CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(0.3f);
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("music/fluid.wav",true);
		_removeListenersSignal->emit();
	}
	else{
		/*const float incrementW = holderWidth / 150.f;
		float newScaleX = ((_timeSprite->getContentSize().width + incrementW) * _timeSprite->getScaleX()) / _timeSprite->getContentSize().width;

		_timeSprite->setScaleX(newScaleX);
		_timeSprite->setContentSize(cocos2d::Size(_timeSprite->getContentSize().width + incrementW, _timeSprite->getContentSize().height));*/

		static_cast<cocos2d::ui::LoadingBar*>(m_pLayer->getChildByTag(3)->getChildByTag(9))->setPercent((timerAccum / 15.f) * 100.f);
	}
}

void LevelLayerHud::onTimeFinish(float delta){
	timerAccum2 += 0.1f;
	if (timerAccum2 > 45.f){
		this->unschedule(schedule_selector(LevelLayerHud::onTimeFinish));
		_pauseFluidSignal->emit(true);

		auto popup = cocos2d::Layer::create();
		cocos2d::ui::Layout* _layout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ui/ColoriterPopup/ColoriterPopup.json"));
		popup->addChild(_layout,100);
		this->addChild(popup);

		cocos2d::ui::Widget *startButton = static_cast<cocos2d::ui::Widget*>(popup->getChildByTag(3)->getChildByTag(4));


		startButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)&LevelLayerHud::goBackButtonAction);

		CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();

		if (FluidUtils::getPlayerScore() >= 75u && FluidUtils::getPlayerScore() < 125u)
			static_cast<cocos2d::ui::Layout*>(popup->getChildByTag(3))->setBackGroundImage("ui/ColoriterPopup/Completed1.png");
		else if (FluidUtils::getPlayerScore() >= 125u && FluidUtils::getPlayerScore() < 200u)
			static_cast<cocos2d::ui::Layout*>(popup->getChildByTag(3))->setBackGroundImage("ui/ColoriterPopup/Completed2.png");
		else if (FluidUtils::getPlayerScore() >= 200u)
			static_cast<cocos2d::ui::Layout*>(popup->getChildByTag(3))->setBackGroundImage("ui/ColoriterPopup/Completed3.png");
	}
}

void LevelLayerHud::goBackButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type){
	switch (type)
	{
	case cocos2d::ui::TOUCH_EVENT_BEGAN:{
		auto scene = LevelSelectorScene::create();
		auto *transition = cocos2d::TransitionFade::create(1, scene);
		cocos2d::Director::getInstance()->replaceScene(scene);
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_MOVED: {
		// TODO
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_ENDED:{
		// TODO
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_CANCELED:{
		// TODO
		break;
	}
	default:{
		// TODO
		break;
	}
	}
}