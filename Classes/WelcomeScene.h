#ifndef __WELCOMESCENE_H__
#define __WELCOMESCENE_H__

#include "cocos2d.h"

class WelcomeScene : public cocos2d::Scene{
public:
	WelcomeScene();
	~WelcomeScene();

	void onEnter() override;
	void onEnterTransitionDidFinish() override;

	bool virtual init() override;
	CREATE_FUNC(WelcomeScene);
};

#endif
