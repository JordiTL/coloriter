#ifndef __LEVELLAYERSELECTOR_H__
#define __LEVELLAYERSELECTOR_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class LevelLayerSelector : public cocos2d::Layer{
protected:
	void level1Action(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type);
	void level2Action(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type);
public:
	LevelLayerSelector();
	~LevelLayerSelector();

	virtual bool init() override;

	CREATE_FUNC(LevelLayerSelector);
};

#endif

