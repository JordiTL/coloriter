#ifndef __LEVELLAYERMAP_H__
#define __LEVELLAYERMAP_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "LevelLayer.h"

class Map;

class LevelLayerMap : public LevelLayer{
private:
	Map * _map;
	b2Body * _bodyBox;
	b2Body * _bodyBox2;
	b2Body * _bodyGear1;
	b2Body * _bodyGear2;
	b2World * _physicsWorld;
	b2BodyDef box1Body;
	int _flag;
public:
	CREATE_FUNC(LevelLayerMap);
	LevelLayerMap();
	~LevelLayerMap();

	virtual void lInit() override;
	virtual void lUpdate(const float delta) override;
	virtual void lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated) override;

	void setMap(Map * tilemap);
	Map * getMap() const;
	void initPhysics();

	void removeListenersReaction();
};

#endif
