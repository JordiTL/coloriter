#ifndef __MAP_H__
#define __MAP_H__

#include <vector>
#include "cocos2d.h"
#include "Collider.h"
#include "PhysicsObject.h"
#include "Box2D/Box2D.h"
#include <string>


#ifndef TILED_MAP_COLLIDER_TAG
#define TILED_MAP_COLLIDER_TAG "Colliders"
#endif

#ifndef TILED_MAP_OBJECTS_TAG
#define TILED_MAP_OBJECTS_TAG "Objects"
#endif

#define MTP_VALUE (32.0f)
#define FM_PI					(3.1415926535f)

class Map{
private:
	cocos2d::TMXTiledMap * _tiledMap;
	cocos2d::Layer * _layer;
	std::vector<Collider> * _colliders;
	std::vector<b2Body *> _b2Bodies;
	std::vector<PhysicsObject> * _objects;

	b2World *_world;

	std::string _filename;
	bool _unloaded;

private:
	void _loadColliders();
	void _loadObjects();

	void _resizePolyCollider(b2Body* body, const float scaleFactorX, const float scaleFactorY, const float originX, const float originY);

public:
	Map();
	~Map();

	bool loadFromFile(const std::string& filename, b2World * world);

	void reload();
	void unload();

	std::vector<Collider>& getColliders() const;
	std::vector<PhysicsObject>& getObjects() const;
	cocos2d::TMXTiledMap& getTiledMap() const;
	cocos2d::Layer& getLayer() const;

	b2World& getPhysicsWorld() const;
	void setPhysicsWorld(b2World * world);

	void setSize(const float width, const float height);

	void updateBehaviours(const float delta);
};


#endif
