#ifndef __WELCOMELAYER_H__
#define __WELCOMELAYER_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class WelcomeLayer : public cocos2d::Layer{
protected:
	void startButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type);
	void closeButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type);
public:
	WelcomeLayer();
	~WelcomeLayer();

	virtual bool init() override;

	CREATE_FUNC(WelcomeLayer);
};

#endif
