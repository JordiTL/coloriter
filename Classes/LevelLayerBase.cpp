#include "LevelLayerBase.h"
#include "FluidManager.h"
#include "GLES-Render.h"
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "MapManager.h"


LevelLayerBase::LevelLayerBase(){
}

LevelLayerBase::~LevelLayerBase(){
}

void LevelLayerBase::lInit(){
	
}

void LevelLayerBase::lUpdate(const float delta){
	
}

void LevelLayerBase::lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated){
}