#include "LevelLayerMap.h"
#include "FluidManager.h"
#include "GLES-Render.h"
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Map.h"
#include "GB2ShapeCache-x.h"

#define PTM_RATIO 32


LevelLayerMap::LevelLayerMap() : _map(nullptr){
}


LevelLayerMap::~LevelLayerMap(){
}

void LevelLayerMap::setMap(Map * tilemap){
	if (_map != nullptr) {
		this->removeChildByTag(0);
		delete _map;
	}
	_map = tilemap;
	_physicsWorld = &tilemap->getPhysicsWorld();
	addChild(&_map->getLayer(), -10, 0);
}

Map * LevelLayerMap::getMap() const{
	return _map;
}

void LevelLayerMap::initPhysics(){

	//**************** BOX1 ***************** 
	auto box1 = cocos2d::Sprite::create("Maps/Dynamics/box1.png");
	box1->setPosition(cocos2d::ccp(720,515));
	this->addChild(box1);

	box1Body;
	box1Body.type = b2_staticBody;
	//box1Body.linearVelocity.Set(1, 0);
	box1Body.position.Set(720 / PTM_RATIO, 515 / PTM_RATIO);
	box1Body.userData = box1;
	_bodyBox = _physicsWorld->CreateBody(&box1Body);
	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile("Maps/Dynamics/box1.plist");
	box1->setAnchorPoint(cocos2d::Point::ZERO);
	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addFixturesToBody(_bodyBox, "box1");
	
	//***************************************  


	//**************** BOX2 ***************** 
	auto box2 = cocos2d::Sprite::create("Maps/Dynamics/box2.png");
	box2->setPosition(cocos2d::ccp(800, 515));
	this->addChild(box2);

	b2BodyDef box2Body;
	box2Body.type = b2_staticBody;
	//box2Body.linearVelocity.Set(1, 0);
	box2Body.position.Set(800 / PTM_RATIO, 515 / PTM_RATIO);
	box2Body.userData = box2;
	_bodyBox2 = _physicsWorld->CreateBody(&box2Body);

	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile("Maps/Dynamics/box2.plist");
	box2->setAnchorPoint(cocos2d::Point::ZERO);
	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addFixturesToBody(_bodyBox2, "box2");
	//***************************************  

	//**************** Gear1 ***************** 
	auto gear1 = cocos2d::Sprite::create("Maps/Dynamics/Gear1.png");
	gear1->setPosition(cocos2d::ccp(300, 150));
	this->addChild(gear1);

	b2BodyDef gear1Body;
	gear1Body.type = b2_staticBody;
	//gear1Body.linearVelocity.Set(1, 0);
	gear1Body.position.Set(300 / PTM_RATIO, 150 / PTM_RATIO);
	gear1Body.userData = gear1;
	_bodyGear1 = _physicsWorld->CreateBody(&gear1Body);

	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile("Maps/Dynamics/Gear1.plist");
	gear1->setAnchorPoint(cocos2d::Point::ZERO);
	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addFixturesToBody(_bodyGear1, "Gear1");
	//***************************************  

	//**************** BOX2 ***************** 
	auto gear2 = cocos2d::Sprite::create("Maps/Dynamics/ramp2.png");
	gear2->setPosition(cocos2d::ccp(910, 515));
	this->addChild(gear2);

	b2BodyDef gear2Body;
	gear2Body.type = b2_staticBody;
	//gear2Body.linearVelocity.Set(1, 0);
	gear2Body.position.Set(910 / PTM_RATIO, 515 / PTM_RATIO);
	gear2Body.userData = gear2;
	_bodyGear2 = _physicsWorld->CreateBody(&gear2Body);

	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile("Maps/Dynamics/ramp2.plist");
	gear2->setAnchorPoint(cocos2d::Point::ZERO);
	cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addFixturesToBody(_bodyGear2, "ramp2");
	//***************************************  

	auto listenerMovement = cocos2d::EventListenerTouchOneByOne::create();

	listenerMovement->setSwallowTouches(true);

	listenerMovement->onTouchBegan = [=](cocos2d::Touch * touch, cocos2d::Event* event)
	{
		auto target = static_cast<cocos2d::Sprite*>(event->getCurrentTarget());

		cocos2d::Point locationInNode = target->convertToNodeSpace(touch->getLocation());
		cocos2d::Size s = target->getContentSize();
		cocos2d::Rect rect = cocos2d::Rect(0, 0, s.width, s.height);
		

		if (target == box1)
		{
			_flag = 1;
			_bodyBox->SetType(b2_dynamicBody);
			_bodyBox->SetAwake(false);
		}
		else if (target == box2)
		{
			_flag = 2;
			_bodyBox2->SetType(b2_dynamicBody);
			_bodyBox2->SetAwake(false);
		}
		else if (target == gear1)
		{
			_flag = 3;
			_bodyGear1->SetType(b2_dynamicBody);
			_bodyGear1->SetAwake(false);
		}
		else if (target == gear2)
		{
			_flag = 4;
			_bodyGear2->SetType(b2_dynamicBody);
			_bodyGear2->SetAwake(false);
		}

		if (rect.containsPoint(locationInNode))
		{
			target->setOpacity(180);
			return true;
		}
		return false;
	};

	listenerMovement->onTouchMoved = [this](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		auto target = static_cast<cocos2d::Sprite*>(event->getCurrentTarget());
		target->setPosition(target->getPosition() + touch->getDelta());

		switch (_flag)
		{
			case 1:
				_bodyBox->SetTransform(b2Vec2(target->getPosition().x / MTP_VALUE, target->getPosition().y / MTP_VALUE), 0.f);
				break;

			case 2:
				_bodyBox2->SetTransform(b2Vec2(target->getPosition().x / MTP_VALUE, target->getPosition().y / MTP_VALUE), 0.f);
				break;
			case 3:
				_bodyGear1->SetTransform(b2Vec2(target->getPosition().x / MTP_VALUE, target->getPosition().y / MTP_VALUE), 0.f);
				break;
			case 4:
				_bodyGear2->SetTransform(b2Vec2(target->getPosition().x / MTP_VALUE, target->getPosition().y / MTP_VALUE), 0.f);
				break;
		}
		

	};

	listenerMovement->onTouchEnded = [this](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		auto target = static_cast<cocos2d::Sprite*>(event->getCurrentTarget());
		target->setOpacity(255);

		switch (_flag)
		{
			case 1:
				_bodyBox->SetAwake(true);			
			break;

			case 2:
				_bodyBox2->SetAwake(true);
				break;
			case 3:
				_bodyGear1->SetAwake(true);
				break;
			case 4:
				_bodyGear2->SetAwake(true);
				break;

		}

	};

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerMovement, box1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerMovement->clone(), box2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerMovement->clone(), gear1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerMovement->clone(), gear2);
}

void LevelLayerMap::lInit(){


}

void LevelLayerMap::lUpdate(const float delta)
{
	cocos2d::Size screenSize = cocos2d::Director::getInstance()->getWinSize();

	cocos2d::Sprite *box1 = (cocos2d::Sprite *)_bodyBox->GetUserData();
	b2Vec2 pos = _bodyBox->GetPosition();
	box1->setPosition(cocos2d::ccp(pos.x * PTM_RATIO, pos.y * PTM_RATIO));
	float rot = _bodyBox->GetAngle();
	box1->setRotation(-1 * rot* 180.0f / M_PI);

	cocos2d::Sprite *box2 = (cocos2d::Sprite *)_bodyBox2->GetUserData();
	b2Vec2 pos2 = _bodyBox2->GetPosition();
	box2->setPosition(cocos2d::ccp(pos2.x * PTM_RATIO, pos2.y * PTM_RATIO));
	float rot2 = _bodyBox2->GetAngle();
	box2->setRotation(-1 * rot2* 180.0f / M_PI);

	cocos2d::Sprite *gear1 = (cocos2d::Sprite *)_bodyGear1->GetUserData();
	b2Vec2 pos3 = _bodyGear1->GetPosition();
	gear1->setPosition(cocos2d::ccp(pos3.x * PTM_RATIO, pos3.y * PTM_RATIO));
	float rot3 = _bodyGear1->GetAngle();
	gear1->setRotation(-1 * rot3* 180.0f / M_PI);

	cocos2d::Sprite *gear2 = (cocos2d::Sprite *)_bodyGear2->GetUserData();
	b2Vec2 pos4 = _bodyGear2->GetPosition();
	gear2->setPosition(cocos2d::ccp(pos4.x * PTM_RATIO, pos4.y * PTM_RATIO));
	float rot4 = _bodyGear2->GetAngle();
	gear2->setRotation(-1 * rot4* 180.0f / M_PI);
}

void LevelLayerMap::lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated)
{

}

void LevelLayerMap::removeListenersReaction(){
	_eventDispatcher->removeAllEventListeners();
}