#include "LoadingLayer.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

LoadingLayer::LoadingLayer(){
}


LoadingLayer::~LoadingLayer(){
	//CCLOG("Loading Layer deleted");
}


bool LoadingLayer::init(){
	if (!Layer::init()){
		return false;
	}
	
	// add background to current scene
	auto *background = cocos2d::Sprite::create("loading_bg.png");
	auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
	auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();
	background->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2);
	this->addChild(background);


	cocos2d::Layer* m_pLayer = cocos2d::Layer::create();
	addChild(m_pLayer);

	cocos2d::ui::Layout* m_pLayout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::shareReader()->widgetFromJsonFile("ui/LoadingScene.json"));

	m_pLayer->addChild(m_pLayout);

	return true;
}