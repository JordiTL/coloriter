#include "LevelScene.h"
#include "LevelLayerMap.h"
#include "LevelLayerFluid.h"
#include "LevelLayerHud.h"
#include "Map.h"


LevelScene::LevelScene(){
}


LevelScene::~LevelScene(){
	CCLOG("Level Scene deleted");
	delete _fluidMgr;
	delete _world;
}

bool LevelScene::init() {
	bool bRet = false;
	do{
		CC_BREAK_IF(!Scene::init());
		bRet = true;
	} while (0);

	return bRet;
}

void LevelScene::loadMap(const std::string& filename)
{
	auto mapLayer = LevelLayerMap::create();
	if (!mapLayer) return;

	


	_world = new b2World(b2Vec2(0.f, -10.f));
	_map.loadFromFile(filename, _world);
	_map.setSize(cocos2d::Director::getInstance()->getWinSize().width, cocos2d::Director::getInstance()->getWinSize().height);
	mapLayer->setMap(&_map);
	mapLayer->initPhysics();
	this->addChild(mapLayer, 10, 1);

	
	auto fluidLayer = LevelLayerFluid::create();
	if (!fluidLayer) return;

	_fluidMgr = new FluidManager(fluidLayer->getBaseLayer(), &_map.getPhysicsWorld());
	fluidLayer->initFluid(_fluidMgr, &_map);

	_pauseFluidSignal.connect(std::bind(&LevelLayerFluid::pauseFluidReaction, fluidLayer, std::placeholders::_1));
	_removeListenersSignal.connect(std::bind(&LevelLayerMap::removeListenersReaction, mapLayer));
	this->addChild(fluidLayer, 20, 2);

	auto hudLayer = LevelLayerHud::create(&_pauseFluidSignal, &_removeListenersSignal);

	this->addChild(hudLayer, 30, 3);
}

void LevelScene::onEnter(){
	Node::onEnter();
}

void LevelScene::onEnterTransitionDidFinish(){
	Node::onEnterTransitionDidFinish();
}

