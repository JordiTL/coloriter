#ifndef __IGAME_H__
#define __IGAME_H__
#include "cocos2d.h"

class IGame{
public:
	virtual void gInit() = 0;
	virtual void gUpdate(const float delta) = 0;
	virtual void gDraw(const kmMat4 &transform, bool transformUpdated) = 0;
};

#endif;