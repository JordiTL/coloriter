#ifndef __LEVELSELECTORSCENE_H__
#define __LEVELSELECTORSCENE_H__

#include "cocos2d.h"

class LevelSelectorScene : public cocos2d::Scene{
public:
	LevelSelectorScene();
	~LevelSelectorScene();

	void onEnter() override;
	void onEnterTransitionDidFinish() override;

	bool virtual init() override;
	CREATE_FUNC(LevelSelectorScene);
};

#endif

