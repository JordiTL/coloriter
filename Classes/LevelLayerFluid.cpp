#include "LevelLayerFluid.h"
#include "FluidManager.h"
#include "GLES-Render.h"
#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Map.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
LevelLayerFluid::LevelLayerFluid(){
}


LevelLayerFluid::~LevelLayerFluid(){
	CCLOG("Level Layer deleted");
	//delete _physicsWorld;
	//_physicsWorld = nullptr;
	//delete m_debugDraw;
	//m_debugDraw = NULL;

	//delete _fluidMgr;
	//_fluidMgr = nullptr;
}

void LevelLayerFluid::lInit(){
	cocos2d::Size s = cocos2d::Director::getInstance()->getWinSize();
	_paused = true;
	const float RenderTextureScale = 0.5f;
	const float RenderFluidScale = 1.0f;



	for (unsigned int i = 0; i < 3; ++i){
		_rTextures.push_back(
			cocos2d::RenderTexture::create(s.width*RenderTextureScale, s.height*RenderTextureScale)
			);



		_rTextures.at(i)->setVirtualViewport(
			cocos2d::Point(0.f, 0.f),
			cocos2d::Rect(.0f, .0f, s.width*0.5f, s.height*0.5f),
			cocos2d::Rect(.0f, .0f, s.width*0.25f, s.height*0.25f)
			);

		_rTextures.at(i)->getSprite()->setPosition(
			cocos2d::Point(s.width / 2, s.height / 2)
			);

		//_rTextures.at(i)->getSprite()->getTexture()->setTexParameters();

		cocos2d::Texture2D::TexParams texParams = {
			GL_LINEAR,
			GL_LINEAR,
			GL_CLAMP_TO_EDGE,
			GL_CLAMP_TO_EDGE
		};

		_rTextures.at(i)->getSprite()->getTexture()->setTexParameters(texParams);

		_rTextures.at(i)->setAutoDraw(true);
		_rTextures.at(i)->getSprite()->setFlippedY(true);


		if (i != 0){
			_rTextures.at(i)->addChild(_rTextures.at(i - 1));
		}
	}

	_rTextures.at(0)->getSprite()->setScale(2.0f);
	_rTextures.at(1)->getSprite()->setScale(2.0f);
	_rTextures.at(2)->getSprite()->setScale(2.0f);

	addChild(_rTextures.at(_rTextures.size() - 1));

	CCLOG("BEGIN LEVEL INIT");
	auto listener1 = cocos2d::EventListenerTouchOneByOne::create();
	// When "swallow touches" is true, then returning 'true' from the onTouchBegan method will "swallow" the touch event, preventing other listeners from using it.
	listener1->setSwallowTouches(true);

	// Example of using a lambda expression to implement onTouchBegan event callback function
	listener1->onTouchEnded = [this](cocos2d::Touch* touch, cocos2d::Event* event){
		CCLOG("END TOUCH");

	};
	listener1->onTouchBegan = [this](cocos2d::Touch* touch, cocos2d::Event* event){
		CCLOG("BEGIN TOUCH");

		return false;
	};
	listener1->onTouchMoved = [this](cocos2d::Touch* touch, cocos2d::Event* event){
		CCLOG("MOVED TOUCH");

	};

	CCLOG("INIT LISTENER");

	_eventDispatcher->addEventListenerWithFixedPriority(listener1, 2);







	// -------------------------------------------------------------------------------------------------------------------------

	cocos2d::GLProgram *shader1 = new cocos2d::GLProgram();
	shader1->initWithVertexShaderFilename("shaders/gaussian_blur_H.vert", "shaders/gaussian_blur_H.frag");
	shader1->addAttribute(cocos2d::kCCAttributeNamePosition, cocos2d::kCCVertexAttrib_Position);
	shader1->addAttribute(cocos2d::kCCAttributeNameTexCoord, cocos2d::kCCVertexAttrib_TexCoords);
	shader1->link();
	shader1->updateUniforms();

	cocos2d::GLProgram *shader2 = new cocos2d::GLProgram();
	shader2->initWithVertexShaderFilename("shaders/gaussian_blur_V.vert", "shaders/gaussian_blur_V.frag");
	shader2->addAttribute(cocos2d::kCCAttributeNamePosition, cocos2d::kCCVertexAttrib_Position);
	shader2->addAttribute(cocos2d::kCCAttributeNameTexCoord, cocos2d::kCCVertexAttrib_TexCoords);
	shader2->link();
	shader2->updateUniforms();

	cocos2d::GLProgram *shader3 = new cocos2d::GLProgram();
	shader3->initWithVertexShaderFilename("shaders/fluid.vert", "shaders/fluid.frag");
	shader3->addAttribute(cocos2d::kCCAttributeNamePosition, cocos2d::kCCVertexAttrib_Position);
	shader3->addAttribute(cocos2d::kCCAttributeNameTexCoord, cocos2d::kCCVertexAttrib_TexCoords);
	shader3->link();
	shader3->updateUniforms();

	cocos2d::CCShaderCache::getInstance()->addProgram(shader1, "GaussianBlurH");
	cocos2d::CCShaderCache::getInstance()->addProgram(shader2, "GaussianBlurV");
	cocos2d::CCShaderCache::getInstance()->addProgram(shader3, "Fluid");

	/// Trace OpenGL error if any
	CHECK_GL_ERROR_DEBUG();

	//---------------------------------------------------------------------------------------------------------------------------

	//_canvas->setShaderProgram(cocos2d::ShaderCache::getInstance()->getProgram("GaussianBlur"));
	_rTextures.at(0)->getSprite()->setShaderProgram(cocos2d::ShaderCache::getInstance()->getProgram("GaussianBlurH"));
	_rTextures.at(1)->getSprite()->setShaderProgram(cocos2d::ShaderCache::getInstance()->getProgram("GaussianBlurV"));
	_rTextures.at(2)->getSprite()->setShaderProgram(cocos2d::ShaderCache::getInstance()->getProgram("Fluid"));


	/*cocos2d::ui::Layout* m_pLayout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ui/Tube/Tube_1.json"));
	m_pLayer->setSwallowsTouches(false);
	m_pLayer->setTouchEnabled(true);
	//m_pLayout->setSwallowsTouches(false);
	m_pLayout->setTouchEnabled(false);
	m_pLayer->addChild(m_pLayout);

	*/
	_fluidLayer = cocos2d::Layer::create();
	_rTextures.at(0)->addChild(_fluidLayer);

}

void LevelLayerFluid::initFluid(FluidManager * fluidMgr, Map * tilemap){

	cocos2d::Size s = cocos2d::Director::getInstance()->getWinSize();
	_fluidMgr = fluidMgr;
	_fluidMgr->setSpawnPosition(b2Vec2(1100.f, 600.f));
	
	

	this->setMap(tilemap);
}


void LevelLayerFluid::lUpdate(const float delta){
	if (!_paused){
		for (auto tex : _rTextures)
			tex->clear(0.0f, 0.0f, 0.0f, 0.0f);

		_fluidMgr->update(delta);
		_map->updateBehaviours(delta);
	}
}

void LevelLayerFluid::lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated){
	
	kmGLPushMatrix();
	//_fluidMgr->draw();
	kmGLPopMatrix();
}

void LevelLayerFluid::setMap(Map * tilemap){
	_map = tilemap;
	cocos2d::Size s = cocos2d::Director::getInstance()->getWinSize();
	_physicsWorld = &tilemap->getPhysicsWorld();
	//auto layer = cocos2d::Layer::create();
	//layer->setPosition(this->getAnchorPoint());

	//layer->getChildByTag(100)->setPosition(s.width / 2, s.height / 2);
	//layer->setScale(1.f);
	//_fluidMgr->setSpawnPosition(b2Vec2(0, 0));
	//_rTextures.at(0)->addChild(_fluidMgr->getLayer());
	
	//_fluidMgr->loadColliders(_map->getColliders());
}

Map * LevelLayerFluid::getMap() const{
	return _map;
}

void LevelLayerFluid::pauseFluidReaction(bool flag){
	_paused = flag;
}
