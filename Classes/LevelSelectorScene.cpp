#include "LevelSelectorScene.h"
#include "LevelLayerSelector.h"
#include "LevelScene.h"


LevelSelectorScene::LevelSelectorScene(){
}


LevelSelectorScene::~LevelSelectorScene(){
	CCLOG("Welcome Scene deleted");
}

bool LevelSelectorScene::init(){
	bool bRet = false;
	do{
		CC_BREAK_IF(!Scene::init());
		auto mainLayer = LevelLayerSelector::create();
		CC_BREAK_IF(!mainLayer);
		this->addChild(mainLayer);
		bRet = true;
	} while (0);

	return bRet;
}

void LevelSelectorScene::onEnter(){
	Node::onEnter();

}

void LevelSelectorScene::onEnterTransitionDidFinish(){
	Node::onEnterTransitionDidFinish();


}
