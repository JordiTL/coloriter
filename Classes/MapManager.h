#ifndef __MAPMANAGER_H__
#define __MAPMANAGER_H__

#include <string>
#include "cocos2d.h"
#include "Collider.h"
#include "Singleton.h"

class MapManager : public Singleton<MapManager>{
	friend class Singleton<MapManager>;

private:
	cocos2d::TMXTiledMap * _map;

private:
	MapManager();
	
public:
	~MapManager();
	void loadMap(const std::string& filename);
	void releaseMap();

	void attachMapTo(cocos2d::Node& node, const int zIndex, const int tag);
	std::vector<Collider> getColliders(const std::string& layerName);
	cocos2d::TMXTiledMap * getTMXMap(){ return _map; }
};

#endif