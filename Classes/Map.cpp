#include "Map.h"
#include "PhysicsObject.h"
#include "GB2ShapeCache-x.h"
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) * 0.01745329252f)

Map::Map() : _tiledMap(nullptr), _colliders(nullptr), _unloaded(false), _world(nullptr), _objects(nullptr), _layer(nullptr){
	_filename = "";
}

Map::~Map(){
	if (_tiledMap != nullptr) _tiledMap->release();
	if (_colliders != nullptr) delete _colliders;
	if (_objects != nullptr) delete _objects;
	//if (_world != nullptr) delete _world;
}

bool Map::loadFromFile(const std::string& filename, b2World * world){
	unload();

	_world = world;
	_filename = filename;
	_tiledMap = cocos2d::TMXTiledMap::create(filename);
	_layer = cocos2d::Layer::create();
	_layer->retain();
	_layer->addChild(_tiledMap);

	if (_tiledMap == nullptr){
		return false;
	}

	_tiledMap->retain();

	_colliders = new std::vector<Collider>();
	_objects = new std::vector<PhysicsObject>();
	_loadColliders();
	_loadObjects();

	return true;
}

void Map::reload(){
	if (_unloaded){
		_tiledMap = cocos2d::TMXTiledMap::create(_filename);
		_layer = cocos2d::Layer::create();
		_layer->retain();
		_colliders = new std::vector<Collider>();
		_objects = new std::vector<PhysicsObject>();
		_loadColliders();
		_loadObjects();

		_unloaded = false;
	}
}

void Map::unload(){
	if (_tiledMap != nullptr){
		_tiledMap->cleanup();
		_tiledMap->release();
		_tiledMap = nullptr;
	}

	if (_layer != nullptr){
		_layer->cleanup();
		_layer->release();
		_layer = nullptr;
	}

	if (_colliders != nullptr){
		_colliders->clear();
		delete _colliders;
		_colliders = nullptr;
	}

	if (_objects != nullptr){
		_objects->clear();
		delete _objects;
		_objects = nullptr;
	}

	if (_world != nullptr){
		delete _world;
		_world = nullptr;
	}

	_unloaded = true;
}

std::vector<Collider>& Map::getColliders() const{
	return *_colliders;
}

cocos2d::TMXTiledMap& Map::getTiledMap() const{
	return *_tiledMap;
}

b2World& Map::getPhysicsWorld() const{
	return *_world;
}

void Map::setPhysicsWorld(b2World * world){
	_world = world;
}

std::vector<PhysicsObject>&  Map::getObjects() const{
	return *_objects;
}

cocos2d::Layer& Map::getLayer() const{
	return *_layer;
}

void Map::_loadColliders(){
	cocos2d::TMXObjectGroup *colGroup = _tiledMap->getObjectGroup(TILED_MAP_COLLIDER_TAG);

	CCAssert(colGroup != nullptr, "Map doesn't contain any collider");

	cocos2d::ValueVector m_pObjects;
	m_pObjects = colGroup->getObjects();
	_colliders->reserve(m_pObjects.size());

	Collider col;
	std::string name, type_name;

	if (m_pObjects.size() > 0){
		cocos2d::ValueVector::iterator it;

		for (it = m_pObjects.begin(); it != m_pObjects.end(); ++it)		 {
			//name = ((*it).asValueMap().at("name")).asString();
			//type_name = ((*it).asValueMap().at("type")).asString();

			col.x = ((*it).asValueMap().at("x")).asFloat();
			col.y = ((*it).asValueMap().at("y")).asFloat();
			col.width = ((*it).asValueMap().at("width")).asFloat();
			col.height = ((*it).asValueMap().at("height")).asFloat();

			cocos2d::ValueMap map = (*it).asValueMap();
			if (map.find("rotation") != map.cend())
				col.rotation = map.at("rotation").asFloat();
			else
				col.rotation = .0f;

			_colliders->push_back(col);
		}
	}

	_world->ClearForces();
	// Physics body creation centered at (0,0)
	b2BodyDef bd;
	bd.type = b2_staticBody;
	bd.position.Set(.0f, .0f);
	b2Body* ground = _world->CreateBody(&bd);

	b2PolygonShape sd;
	b2FixtureDef gFix;
	gFix.shape = &sd;

	for (auto collider : *_colliders){
		float cosa = std::cos(-collider.rotation* M_PI / 180.f);
		float sina = std::sin(-collider.rotation* M_PI / 180.f);


		float hx = (collider.width / 2) / MTP_VALUE;
		float hy = (collider.height / 2) / MTP_VALUE;
		float cx = (collider.x + (collider.width / 2)*cosa - (collider.height / 2)*sina) / MTP_VALUE;
		float cy = (collider.y + (collider.width / 2)*sina + (collider.height / 2)*cosa) / MTP_VALUE;

		sd.SetAsBox(hx, hy, b2Vec2(cx, cy), -collider.rotation * M_PI / 180.f);
		ground->CreateFixture(&gFix);
	}




}

void Map::_loadObjects(){
	cocos2d::TMXObjectGroup *colGroup = _tiledMap->getObjectGroup(TILED_MAP_OBJECTS_TAG);

	CCAssert(colGroup != nullptr, "Map doesn't contain any collider");

	cocos2d::ValueVector m_pObjects;
	m_pObjects = colGroup->getObjects();
	_objects->reserve(m_pObjects.size());

	PhysicsObject col;
	std::string name, type_name;

	if (m_pObjects.size() > 0){
		cocos2d::ValueVector::iterator it;

		for (it = m_pObjects.begin(); it != m_pObjects.end(); ++it)		 {
			//name = ((*it).asValueMap().at("name")).asString();
			//type_name = ((*it).asValueMap().at("type")).asString();
			cocos2d::ValueMap& map = (*it).asValueMap();
			col.x = (map.at("x")).asFloat();
			col.y = (map.at("y")).asFloat();
			col.width = (map.at("width")).asFloat();
			col.height = (map.at("height")).asFloat();

			col.filename = (map.at("filename")).asString();
			col.rotationSpeed = (map.at("rotationSpeed")).asFloat();
			col.translationSpeed = (map.at("translationSpeed")).asFloat();
			col.physicsDefFilename = (map.at("physicsDef")).asString();
			col.physicsId = (map.at("physicsId")).asString();
			if (map.find("rotation") != map.cend())
				col.rotation = map.at("rotation").asFloat();
			else
				col.rotation = .0f;

			_objects->push_back(col);
		}
	}


	std::string prefix = "Maps/Objects/";

	unsigned int cont=0;

	for (auto object : *_objects){
		std::string filePath(prefix);
		filePath.append(object.filename);
		std::string filePathPhysics(prefix);
		filePathPhysics.append(object.physicsDefFilename);

		cocos2d::Sprite * sp = cocos2d::Sprite::create(filePath);

		float newScaleX = (object.width * sp->getScaleX()) / sp->getContentSize().width;
		float newScaleY = (object.height * sp->getScaleY()) / sp->getContentSize().height;

		sp->setScaleX(newScaleX);
		sp->setScaleY(newScaleY);

		cocos2d::Texture2D::TexParams texParams = {
			GL_LINEAR_MIPMAP_LINEAR,
			GL_LINEAR,
			GL_CLAMP_TO_EDGE,
			GL_CLAMP_TO_EDGE
		};

		sp->getTexture()->setTexParameters(texParams);
		sp->getTexture()->generateMipmap();

		sp->setPosition(object.x + object.width*0.5f, object.y + object.height*0.5f);
		_objects->at(cont).userTag = cont;
		_layer->addChild(sp, 40,cont);


		cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addShapesWithFile(filePathPhysics);

		b2BodyDef bodyDef;
		bodyDef.type = b2_kinematicBody;
		bodyDef.position.Set(
			(object.x + object.width / 2) / MTP_VALUE,
			(object.y + object.height / 2) / MTP_VALUE
			);
		
		b2Body* body = _world->CreateBody(&bodyDef);

		cocos2d::GB2ShapeCache::sharedGB2ShapeCache()->addFixturesToBody(body, _objects->at(cont).physicsId);
		
		_b2Bodies.push_back(body);

		_resizePolyCollider(body, sp->getScaleX(), sp->getScaleY(), body->GetLocalCenter().x, body->GetLocalCenter().y);

		cont++;
	}
}

void Map::_resizePolyCollider(b2Body* body, const float scaleFactorX, const float scaleFactorY, const float originX, const float originY){
	b2Fixture * fixtureList = body->GetFixtureList();

	for (fixtureList; fixtureList; fixtureList = fixtureList->GetNext()){
		b2PolygonShape* pShape = static_cast<b2PolygonShape*>(fixtureList->GetShape());

		const unsigned int vCount = pShape->GetVertexCount();

		for (int i = 0; i < vCount; ++i){
			b2Vec2& vertex = pShape->m_vertices[i];

			vertex.Set(
				(originX + ((vertex.x - originX)*scaleFactorX)),
				(originY + ((vertex.y - originY)*scaleFactorY))
				);
		}
	}

}

void Map::setSize(const float width, const float height){
	CCAssert(_tiledMap != nullptr, "No tiledmap loaded yet...");
	
	float newScaleX = (width * _tiledMap->getScaleX()) / _tiledMap->getContentSize().width;
	float newScaleY = (height * _tiledMap->getScaleY()) / _tiledMap->getContentSize().height;

	_tiledMap->setScaleX(newScaleX);
	_tiledMap->setScaleY(newScaleY);
}

void Map::updateBehaviours(const float delta){
	for (auto object : *_objects){
		cocos2d::Node * child = _layer->getChildByTag(object.userTag);

		child->setRotation(child->getRotation() + object.rotationSpeed*delta);

		float32 b2Angle = -1 * DEGREES_TO_RADIANS(child->getRotation());
		_b2Bodies.at(object.userTag)->SetTransform(_b2Bodies.at(object.userTag)->GetPosition(), b2Angle);
		//_b2Bodies.at(object.userTag)->SetAngularVelocity(-object.rotationSpeed*M_PI / 180.f));
	}
}