#include "MapManager.h"


MapManager::MapManager() :_map(nullptr){
}


MapManager::~MapManager(){
}


void MapManager::loadMap(const std::string& filename){
	if (_map != nullptr) releaseMap();
	_map = cocos2d::TMXTiledMap::create(filename);
	_map->retain();
}

void MapManager::releaseMap(){
	if (_map != nullptr) {
		_map->release();
		_map = nullptr;
	}
}

void MapManager::attachMapTo(cocos2d::Node& node, const int zIndex, const int tag){
	if (_map != nullptr) node.addChild(_map, zIndex, tag);
}

std::vector<Collider> MapManager::getColliders(const std::string& layerName){
	assert(_map != nullptr);

	cocos2d::TMXObjectGroup *colliders = _map->getObjectGroup(layerName);

	assert(colliders != nullptr);

	std::vector<Collider> result;
	cocos2d::ValueVector m_pObjects;
	m_pObjects = colliders->getObjects();
	result.reserve(m_pObjects.size());

	Collider col;
	cocos2d::String name, type_name;

	if (m_pObjects.size() > 0){
		cocos2d::ValueVector::iterator it;

		for (it = m_pObjects.begin(); it != m_pObjects.end(); ++it)		 {
			//name = ((*it).asValueMap().at("name")).asString();
			//type_name = ((*it).asValueMap().at("type")).asString();
			
			col.x = ((*it).asValueMap().at("x")).asFloat();
			col.y = ((*it).asValueMap().at("y")).asFloat();
			col.width = ((*it).asValueMap().at("width")).asFloat();
			col.height = ((*it).asValueMap().at("height")).asFloat();
			
			
			if ((*it).asValueMap().find("rotation") != (*it).asValueMap().cend())
				col.rotation = ((*it).asValueMap().at("rotation")).asFloat();
			else
				col.rotation = .0f;

			result.push_back(col);
		}
	}

	return result;
}
