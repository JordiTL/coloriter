#ifndef __COLLIDER_H__
#define __COLLIDER_H__

#include "Box2D/Box2D.h"

class Collider{
public:
	float x;
	float y;
	float width;
	float height;
	float rotation;

	Collider(float px = .0f, float py = .0f, float w = 1.f, float h = 1.f, float r = .0f) : x(px), y(py), width(w), height(h), rotation(r){}

	~Collider(){}

	const b2Vec2 topLeft(){ return b2Vec2(x, y + height); }
	const b2Vec2 topRight(){ return b2Vec2(x + width, y + height); }
	const b2Vec2 bottomLeft(){ return b2Vec2(x, y); }
	const b2Vec2 bottomRight(){ return b2Vec2(x + width, y); }
};

#endif
