#include "WelcomeLayer.h"
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "LevelScene.h"
#include "LevelSelectorScene.h"

WelcomeLayer::WelcomeLayer(){
}


WelcomeLayer::~WelcomeLayer(){
	CCLOG("Welcome Layer deleted");
}


bool WelcomeLayer::init(){
	if (!Layer::init()){
		return false;
	}

	cocos2d::Layer* m_pLayer = cocos2d::Layer::create();
	addChild(m_pLayer);

	cocos2d::ui::Layout* m_pLayout = dynamic_cast<cocos2d::ui::Layout*>(cocostudio::GUIReader::getInstance()->widgetFromJsonFile("ui/Main2UI/NewUI_1.json"));

	m_pLayer->addChild(m_pLayout);
	cocos2d::ui::Widget *startButton = static_cast<cocos2d::ui::Widget*>(m_pLayout->getChildByName("Fondo")->getChildByName("StartButton"));
	cocos2d::ui::Widget *closeButton = static_cast<cocos2d::ui::Widget*>(m_pLayout->getChildByName("Fondo")->getChildByName("CloseButton"));

	
	startButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)&WelcomeLayer::startButtonAction);
	closeButton->addTouchEventListener(this, (cocos2d::ui::SEL_TouchEvent)&WelcomeLayer::closeButtonAction);
	return true;
}

void WelcomeLayer::startButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type){
	switch (type)
	{
	case cocos2d::ui::TOUCH_EVENT_BEGAN:{
		auto scene = LevelSelectorScene::create();
		auto *transition = cocos2d::TransitionFade::create(1, scene);
		cocos2d::Director::getInstance()->replaceScene(scene);
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_MOVED: {
		// TODO
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_ENDED:{
		// TODO
		break;
	}
	case cocos2d::ui::TOUCH_EVENT_CANCELED:{
		// TODO
		break;
	}
	default:{
		// TODO
		break;
	}
	}
}

void WelcomeLayer::closeButtonAction(cocos2d::Ref *pSender, cocos2d::ui::TouchEventType type){
	switch (type)
	{
	case cocos2d::ui::TOUCH_EVENT_BEGAN:{
											//exit(0);
											cocos2d::Director::getInstance()->end();
	}
	case cocos2d::ui::TOUCH_EVENT_MOVED: {
											 // TODO
											 break;
	}
	case cocos2d::ui::TOUCH_EVENT_ENDED:{
											// TODO
											break;
	}
	case cocos2d::ui::TOUCH_EVENT_CANCELED:{
											   // TODO
											   break;
	}
	default:{
				// TODO
				break;
	}
	}
}