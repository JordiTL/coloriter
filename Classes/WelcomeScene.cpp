#include "WelcomeScene.h"
#include "WelcomeLayer.h"
#include "LevelScene.h"


WelcomeScene::WelcomeScene(){
}


WelcomeScene::~WelcomeScene(){
	CCLOG("Welcome Scene deleted");
}

bool WelcomeScene::init(){
	bool bRet = false;
	do{
		CC_BREAK_IF(!Scene::init());
		auto mainLayer = WelcomeLayer::create();
		CC_BREAK_IF(!mainLayer);
		this->addChild(mainLayer);
		bRet = true;
	} while (0);

	return bRet;
}

void WelcomeScene::onEnter(){
	Node::onEnter();

}

void WelcomeScene::onEnterTransitionDidFinish(){
	Node::onEnterTransitionDidFinish();
	

}

