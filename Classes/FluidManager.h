/*
 * FluidManager.h
 *
 *  Created on: 31/01/2014
 *      Author: Jorge
 */

#ifndef FLUIDMANAGER_H_
#define FLUIDMANAGER_H_

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "FluidHashList.h"
#include "GLES-Render.h"
#include "Collider.h"

#define nParticles		(200)
#define hashWidth		(40)
#define hashHeight		(40)

const int nominalNeighbourListLength = 128;

#define MTP_VALUE (32.0f)
#define FM_TWO_PI				(6.2831853071f)
#define FM_HALF_PI				(1.5707963267f)
#define FM_PI					(3.1415926535f)
#define FM_THREE_HALF_PI		(4.7123889803f)
/**
 *
 * Struct to put all information about any single particle in the system.
 * It contains some properties like position, velocity, etc.
 *
 */
struct sParticle {
	sParticle() :
			Position(0, 0), LastPosition(0, 0), Velocity(0, 0), Acceleration(0,
					0), Force(0, 0), Mass(0.2f), Restitution(0.3f), Friction(
					0.0f) , Eaten(false){
	}
	~sParticle() {
	}

	b2Vec2 Position;
	b2Vec2 LastPosition;
	b2Vec2 Velocity;
	b2Vec2 Acceleration;
	b2Vec2 Force;
	cocos2d::Sprite *sp;

	
	float Mass;
	float Restitution;
	float Friction;

	bool Eaten;
};

class FluidUtils{
public:
	static unsigned int _playerScore;

public:
	static void setPlayerScore(const unsigned int score){ _playerScore = score; }
	static unsigned int getPlayerScore(){ return _playerScore; }

	static bool particleSolidCollision(b2Fixture* fixture, b2Vec2& particlePos, b2Vec2& nearestPos, b2Vec2& impactNormal, sParticle * particle);
	static void separateParticleFromBody(int particleIdx, b2Vec2& nearestPos, b2Vec2& normal, sParticle *liquid);

	static b2Vec2 world2Screen(const float kMTP, const float maxScreenX, const float maxScreenY, const float posX, const float posY);
	static b2Vec2 screen2World(const float kMTP, const float maxScreenX, const float maxScreenY, const float posX, const float posY);
	static b2Vec2 world2Screen(const float kMTP, const b2Vec2 maxScreen, const b2Vec2 pos);
	static b2Vec2 screen2World(const float kMTP, const b2Vec2 maxScreen, const b2Vec2 pos);
	static b2Vec2 world2Screen(const float kMTP, const float maxScreenX, const float maxScreenY, const b2Vec2 pos);
	static b2Vec2 screen2World(const float kMTP, const float maxScreenX, const float maxScreenY, const b2Vec2 pos);
	static b2Vec2 world2Screen(const float kMTP, const b2Vec2 maxScreen, const float posX, const float posY);
	static b2Vec2 screen2World(const float kMTP, const b2Vec2 maxScreen, const float posX, const float posY);
};

class FluidManager {
private:

	/**
	 * Nested Class to execute a callback after the query of world interactions
	 */
	class QueryWorldInteractions: public b2QueryCallback {
	public:
		QueryWorldInteractions(cFluidHashList (*grid)[hashHeight],
				sParticle *particles) {
			hashGridList = grid;
			liquid = particles;
		}

		bool ReportFixture(b2Fixture* fixture);
		int x, y;
		float deltaT;

	protected:
		cFluidHashList (*hashGridList)[hashHeight];
		sParticle *liquid;
	};

	/**
	 * Nested Class to execute a callback after the query of intersections
	 */
	class QueryWorldPostIntersect: public b2QueryCallback {
	public:
		QueryWorldPostIntersect(cFluidHashList (*grid)[hashHeight],
				sParticle *particles) {
			hashGridList = grid;
			liquid = particles;
		}

		bool ReportFixture(b2Fixture* fixture);
		int x, y;
		float deltaT;

	protected:
		cFluidHashList (*hashGridList)[hashHeight];
		sParticle *liquid;
	};


	/**
	 * FluidManager Private Members
	 */
	cocos2d::SpriteBatchNode *_particleSprites;

	b2World *_world;
	QueryWorldInteractions *_intersectQueryCallback;
	QueryWorldPostIntersect *_eulerIntersectQueryCallback;

	GLESDebugDraw *_debugDraw;		// strong ref

	b2Vec2 _spawnLocation;

	// MAGIC NUMBERS
	float _totalMass;

	float _boxWidth;
	float _boxHeight;

	float _rad;
	float _visc;
	float _idealRad;

	static const float _fluidMinX;
	static const float _fluidMaxX;
	static const float _fluidMinY;
	static const float _fluidMaxY;

	//////////////////////////////////////////////////////////////////////////

	sParticle _liquid[nParticles];
	float _vlenBuffer[nParticles];
	cFluidHashList _hashGridList[hashWidth][hashHeight];

	b2Body* _bod;

	b2Shape *_neighboursBuffer[nominalNeighbourListLength];
	cocos2d::Node *_layer;

private:
	void _clearHashGrid();
	void _hashLocations();
	void _resetGridTailPointers(int particleIdx);
	void _applyLiquidConstraint(float deltaT);

	/**
	*	Checks if some particle is out of the boundaries and then reset it
	*/
	void _checkBounds();
	void _dampenLiquid();
	void _processWorldInteractions(float deltaT);
	void _stepFluidParticles(float deltaT);
	void _resolveIntersections(float deltaT);
	void _createStaticGeometry();


public:
	FluidManager(cocos2d::Node *layer, b2World * world);
	virtual ~FluidManager();

	void draw();
	void update(float dt);

	void setSpawnPosition(const b2Vec2 pos);
	void loadColliders(const std::vector<Collider>& colliders);
	cocos2d::SpriteBatchNode * getParticlesNode(){
		return _particleSprites;
	}

	cocos2d::Node * getLayer(){
		return _layer;
	}


	static inline float mapValue(float val, float minInput, float maxInput,
			float minOutput, float maxOutput) {
		float result = (val - minInput) / (maxInput - minInput);
		result *= (maxOutput - minOutput);
		result += minOutput;
		return result;
	}

	static inline int hashX(float x) {
		float f = mapValue(x, _fluidMinX, _fluidMaxX, 0, hashWidth - .001f);
		return (int) f;
	}

	static inline int hashY(float y) {
		float f = mapValue(y, _fluidMinY, _fluidMaxY, 0, hashHeight - .001f);
		return (int) f;
	}

	static inline float random(float lo, float hi) {
		return ((hi - lo) * CCRANDOM_0_1() + lo);
	}

};

#endif /* FLUIDMANAGER_H_ */
