#ifndef __LEVELLAYERFLUID_H__
#define __LEVELLAYERFLUID_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "LevelLayer.h"

class FluidManager;
class Map;
class LevelLayerFluid : public LevelLayer{
private:
	b2World * _physicsWorld;					// strong ref
	FluidManager * _fluidMgr;
	Map * _map;

	std::vector<cocos2d::RenderTexture*> _rTextures;
	cocos2d::Layer* _fluidLayer;
	bool _paused;

public:
	static LevelLayerFluid* create(){
		LevelLayerFluid *pRet = new LevelLayerFluid();
		if (pRet && pRet->init()){
			pRet->autorelease();
			return pRet;
		}
		else{
			delete pRet;
			pRet = NULL;
			return NULL;
		}
	}
	LevelLayerFluid();
	~LevelLayerFluid();

	virtual void lInit() override;
	virtual void lUpdate(const float delta) override;
	virtual void lDraw(cocos2d::Renderer *renderer, const kmMat4 &transform, bool transformUpdated) override;

	void initFluid(FluidManager * fluidMgr, Map * tilemap);

	void setMap(Map * tilemap);
	Map * getMap() const;

	void pauseFluidReaction(bool flag);

	cocos2d::Layer* getBaseLayer(){ return _fluidLayer; }
};


#endif
