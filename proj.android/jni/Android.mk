LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/CCSignal.cpp \
                   ../../Classes/FluidManager.cpp \
                   ../../Classes/GB2ShapeCache-x.cpp \
                   ../../Classes/GLES-Render.cpp \
                   ../../Classes/LevelLayer.cpp \
                   ../../Classes/LevelLayerBase.cpp \
                   ../../Classes/LevelLayerFluid.cpp \
                   ../../Classes/LevelLayerHud.cpp \
                   ../../Classes/LevelLayerMap.cpp \
                   ../../Classes/LevelLayerSelector.cpp \
                   ../../Classes/LevelScene.cpp \
                   ../../Classes/LevelSelectorScene.cpp \
                   ../../Classes/LoadingLayer.cpp \
                   ../../Classes/LoadingScene.cpp \
                   ../../Classes/Map.cpp \
                   ../../Classes/MapManager.cpp \
                   ../../Classes/WelcomeLayer.cpp \
                   ../../Classes/WelcomeScene.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocostudio_static


include $(BUILD_SHARED_LIBRARY)

$(call import-module,2d)
$(call import-module,audio/android)
$(call import-module,Box2D)
$(call import-module,editor-support/cocostudio)
$(call import-module,extensions)
