attribute vec4 a_position;
attribute vec2 a_texCoord;
 
varying vec2 v_texCoord;
varying vec2 v_blurTexCoords[14];
 
#define multiplier *0.2f
 
void main()
{
	gl_Position = CC_MVPMatrix * a_position;
 
    v_texCoord = a_texCoord;
    v_blurTexCoords[ 0] = v_texCoord + vec2(0.0, -0.028) multiplier;
    v_blurTexCoords[ 1] = v_texCoord + vec2(0.0, -0.024) multiplier;
    v_blurTexCoords[ 2] = v_texCoord + vec2(0.0, -0.020) multiplier;
    v_blurTexCoords[ 3] = v_texCoord + vec2(0.0, -0.016) multiplier;
    v_blurTexCoords[ 4] = v_texCoord + vec2(0.0, -0.012) multiplier;
    v_blurTexCoords[ 5] = v_texCoord + vec2(0.0, -0.008) multiplier;
    v_blurTexCoords[ 6] = v_texCoord + vec2(0.0, -0.004) multiplier;
    v_blurTexCoords[ 7] = v_texCoord + vec2(0.0,  0.004) multiplier;
    v_blurTexCoords[ 8] = v_texCoord + vec2(0.0,  0.008) multiplier;
    v_blurTexCoords[ 9] = v_texCoord + vec2(0.0,  0.012) multiplier;
    v_blurTexCoords[10] = v_texCoord + vec2(0.0,  0.016) multiplier;
    v_blurTexCoords[11] = v_texCoord + vec2(0.0,  0.020) multiplier;
    v_blurTexCoords[12] = v_texCoord + vec2(0.0,  0.024) multiplier;
    v_blurTexCoords[13] = v_texCoord + vec2(0.0,  0.028) multiplier;
}