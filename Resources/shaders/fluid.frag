precision mediump float;

uniform sampler2D u_texture;
 
varying vec2 v_texCoord;
 
#define GRADIENT_MULT 0.8f
#define ALPHA_THRESHOLD 0.01f

void main()
{
	if(texture2D(u_texture, v_texCoord).w > ALPHA_THRESHOLD){
		float outputColor = 
			mix(0.0f,0.1f,clamp(texture2D(u_texture, v_texCoord).w*GRADIENT_MULT, 0.0f, 1.0f));
		gl_FragColor = vec4(outputColor,clamp(0.8f+outputColor, 0.0f, 1.0f),outputColor, 1.0f);
	}else
		gl_FragColor = vec4(0.0f);
}